# Spike Report

## SPIKE TITLE - Packaging an Executable

### Introduction

Now that the game in a testable state we need to learn how to package the game
so that it may be distributed to those who are not in our development team.
We need to learn how to package the game into an Executable file.

### Goals

To learn how to "Cook" in Unreal Engine
To learn how to package a game into an executable

### Personnel

* Primary - Matthew Louden
* Secondary - N/A

### Technologies, Tools, and Resources used

* [how to package a game for Windows in UE4] (https://odederell3d.blog/2019/06/13/ue4-package-a-project-for-windows/)
* [how to package and cook a game in UE4] (https://docs.unrealengine.com/en-US/Engine/Deployment/index.html)

### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

1. Build the level to compile the all components within the level
	2. Select Settings > Project Settings > Description
		3. Set a 192x192 png thumbnail for your project
	2. Move to the Maps & Modes tab
		3. Select the starting map for your game
	2. Move to the Packaging tab
		3. Choose your build configuration (I chose 'Shipping')
	2. Move to the Supported Platforms tab
		3. Select 'Windows'
1. To Packadge the project select File > Package Project > Windows > Windows (64bit)

### What we found out

We found out how to bake maps & compile objects in preparation for packaging & export.
We found out how to package a project for user distribution in UE4
